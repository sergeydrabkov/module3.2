﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Module3_2
{
    public class Program
    {
        protected Program() { }
        static void Main(string[] args)
        {
            Task4 t4 = new Task4();
            int r;
            if (t4.TryParseNaturalNumber("1", out r))
            {
                foreach (int a in t4.GetFibonacciSequence(r))
                {
                    Console.WriteLine(a);
                }
            }

            Task5 t5 = new Task5();
            Console.WriteLine(t5.ReverseNumber(12));

            Task6 t6 = new Task6();
            int[] arr = t6.GenerateArray(-13);
            foreach (int a in arr)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine("------------");
            foreach (int a in t6.UpdateElementToOppositeSign(arr))
            {
                Console.WriteLine(a);
            }

            Task7 t7 = new Task7();
            foreach (int a in t7.FindElementGreaterThenPrevious(new int[] { 3, 9, 8, 4, 5, 1 }))
            {
                Console.WriteLine(a);
            }

            Task8 t8 = new Task8();
            int n = 4;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    Console.WriteLine(t8.FillArrayInSpiral(n)[i, j]);
                    if (j == n - 1) Console.WriteLine("\n");
                }
            }
        }
    }

    public class Task4
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            result = 0;
            bool flag = false;
            uint uresult;
            if (uint.TryParse(input, out uresult))
            {
                result = (int)uresult;
                flag = true;
            }

            return flag;
        }

        public int[] GetFibonacciSequence(int n)
        {
            int[] l = new int[n];

            if (n == 1)
            {
                l[0] = 0;
            }
            else if(n == 2)
            {
                l[0] = 0;
                l[1] = 1;
            }
            else if (n >= 3)
            {
                l[0] = 0;
                l[1] = 1;
                for (int i = 2; i < n; i++)
                {
                    l[i] = l[i - 2] + l[i - 1];
                }
            }

            return l;
        }
    }

    public class Task5
    {
        public int ReverseNumber(int sourceNumber)
        {
            
            bool negative = sourceNumber < 0;
            char[] inputarray;

            if (negative)
            {
                inputarray = (-sourceNumber).ToString().ToCharArray();
            }
            else
            {
                inputarray = sourceNumber.ToString().ToCharArray();
            }
            Array.Reverse(inputarray);            
            
            string output = new string(inputarray);
           
            int result = int.Parse(output);
            
            if (negative) 
            {
                result = -result;
            }
            return result;
        }
    }

    public class Task6
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] a = new int[Math.Abs(size)];
            Random rnd = new Random();

            for (int i = 0; i < Math.Abs(size); i++)
            {
                a[i] = rnd.Next(int.MinValue, int.MaxValue);
            }

            return a;
        }

        public int[] UpdateElementToOppositeSign(int[] source)
        {
            int[] a = new int[source.Length]; 
            for(int i = 0; i< source.Length; i++) 
            {
                a[i] = -source[i];            
            }

            return a;
        }
    }

    public class Task7
    {
        /// <summary>
        /// Use this method to generate array. It shouldn't throws exception.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int[] GenerateArray(int size)
        {
            int[] a = new int[Math.Abs(size)];
            Random rnd = new Random();

            for (int i = 0; i < Math.Abs(size); i++)
            {
                a[i] = rnd.Next(int.MinValue, int.MaxValue);
            }

            return a;
        }

        public List<int> FindElementGreaterThenPrevious(int[] source)
        {
            List<int> l = new List<int>();

            for (int i = 1; i < source.Length; i++)
            {
                if (source[i-1] < source[i])
                {
                    l.Add(source[i]);
                }
            }

            return l;
        }
    }

    public class Task8
    {
        public int[,] FillArrayInSpiral(int size)
        {
            int[,] a = new int[size, size];

            int i = 1, j, k, p = size / 2;
            for (k = 1; k <= p; k++)
            {
                for (j = k - 1; j < size - k + 1; j++) a[k - 1, j] = i++;
                for (j = k; j < size - k + 1; j++) a[j, size - k] = i++;
                for (j = size - k - 1; j >= k - 1; --j) a[size - k, j] = i++;
                for (j = size - k - 1; j >= k; j--) a[j, k - 1] = i++;
            }
            if (size % 2 == 1) a[size, size] = size * size;

            return a;
        }
    }
}
